package main_controll;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import server.Counter;
import server.Customer;
import server.Scheduler;

import java.io.IOException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;


public class Simulation extends Thread{

    private int nrOfClients;
    private int nrOfCounters;
    private int simulationTime;
    private int minArrivalTime;
    private int maxArrivalTime;
    private int minServiceTime;
    private int maxServiceTime; 
   
    private String outFile;
    private Scheduler scheduler;
    private AtomicInteger totalWaitingTime = new AtomicInteger(0);
    private double average=0;
    
    public void Read(String inFile) //read input data from file
    {
        Scanner in = null;
        try 
        {
            in = new Scanner (new File(inFile));
        }
        catch(Exception e) 
        {
            System.out.println("Input file not found!");
        }

        nrOfClients = in.nextInt();
        nrOfCounters = in.nextInt();
        simulationTime=in.nextInt();
        String[] token = in.next().split(",");
        minArrivalTime = Integer.parseInt(token[0]);
        maxArrivalTime = Integer.parseInt(token[1]);
        String[] token2 = in.next().split(",");
        minServiceTime = Integer.parseInt(token2[0]);
        maxServiceTime = Integer.parseInt(token2[1]);

        in.close();
    }
    
    
    public void fileWrite(String outFile,String string) //write output data into file
    {
        try
        ( 
            FileWriter fw = new FileWriter(outFile, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println(string);
        }

        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
    
  
    public Simulation(String inPut, String outPut) //constructor
    {
    	this.outFile = outPut;
        Read(inPut);
        scheduler = new Scheduler();

		for (int i = 0; i < nrOfCounters; i++) {
			Counter counter = new Counter(i+1);
			scheduler.addCounter(counter);
		}
    }
    
   
    private ArrayList<Customer> qwait=new ArrayList<Customer>(); //waiting clients

    public void showClient(Customer c)  // show format: (id,aT,sT)
    {
    	int id=c.getNoId();
    	int arrTime=c.getArrivalTime();
    	int servTime=c.getServiceTime();
    	
    	 fileWrite (this.outFile,"("+ id +","+arrTime+","+servTime+")" );
         System.out.println( "("+ id +","+arrTime+","+servTime+")" );
    }
    
    public void initWaitingList() //generate clients
    {
    	Generator g=new Generator(nrOfClients,minArrivalTime,maxArrivalTime,minServiceTime,maxServiceTime);
    	g.generateClients();
    	qwait=g.getClients();
    	
    }
    
    public void manageClients(int currentTime) // manage waiting clients
    {
    	for(Customer c:qwait) 
	    {     
			if(currentTime ==0) totalWaitingTime.addAndGet(c.getServiceTime());
			
			if(c.getArrivalTime() > currentTime && c.getServiceTime()>=1) showClient(c); //show waiting client
			else if(c.getArrivalTime() == currentTime)
	        {
	        	scheduler.sendClientToCounter(c); //add client to Counter
	        	scheduler.serveClients(); //start thread, wait for serviceTime to be over, then remove 
	        }
	    }
    }
    
    public void manageQueues(int currentTime) //manage clients that are in queues
    {
    	for (Counter c : scheduler.getCounters()) 
    	{
			for(Customer c1:c.getClientsArray()){
				
				if(c1.getArrivalTime() < currentTime && c1.getServiceTime()>=1)
				{
					if(c1.getServiceTime()==1) {c1.setServiceTime(0); c.removeClient(c1);}
					
					else c1.setServiceTime(c1.getServiceTime()-1); 
				}
			}
            if(c.isEmpty()==true)   //empty queue               
            {
                fileWrite(this.outFile,"Queue "+c.getID()+": closed");
                System.out.println("Queue "+c.getID()+": closed");
            }     
            else if(c.isEmpty()==false)   //non-empty queue        
            {
                fileWrite(this.outFile,"Queue " + c.getID() + ":");
                System.out.println("Queue " + c.getID() + ":");
     
                for(Customer c1:c.getClientsArray()){
            	    
                	if(c1.getArrivalTime() <= currentTime && c1.getServiceTime()>=1) showClient(c1);
            	    
			    }         
            }
            
        }
    }
   
    @Override
    public void run() 
    {
        initWaitingList();// place all clients in the waiting list

        int currentTime = 0;
		while (currentTime < simulationTime) 
		{	
			System.out.println("\nTime:"+currentTime);
		    System.out.println("Waiting clients: ");
		    fileWrite(this.outFile,"\nTime:"+currentTime);
		    fileWrite(this.outFile,"Waiting clients: ");
		    
		    manageClients(currentTime); 
		    
		    manageQueues(currentTime);
		
			try{
				  Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			currentTime++;
		}
        average = totalWaitingTime.doubleValue()/nrOfClients;
        fileWrite(this.outFile,"\n\nAverage Waiting time="+average);
        System.out.println("\n\nAverage Waiting time="+average);
        
        System.exit(0);   
    }
}

