package main_controll;
import java.util.ArrayList;
import java.util.Random;

import server.Customer;


public class Generator {

	private int nrOfClients;
	private ArrayList<Customer> clients = new ArrayList<Customer>(nrOfClients);
	private int minArrivalTime;
	private int maxArrivalTime;
	private int minServiceTime;
	private int maxServiceTime;

	public Generator(int nrclients, int minArrivalTime, int maxArrivalTime,
			int minServiceTime, int maxServiceTime) {
		
		super();
		this.nrOfClients=nrclients;
		this.minArrivalTime = minArrivalTime;
		this.maxArrivalTime = maxArrivalTime;
		this.minServiceTime = minServiceTime;
		this.maxServiceTime = maxServiceTime;
	}

	public void generateClients() {
		
		Random rand = new Random();
		int minA = this.minArrivalTime;
		int maxA = this.maxArrivalTime;
		int minS = this.minServiceTime;
		int maxS = this.maxServiceTime;
		
		for (int i = 0; i < nrOfClients; i++) {
			
			int arrTime = minA + rand.nextInt(maxA - minA);
			int servTime = minS + rand.nextInt(maxS - minS);
			
			Customer c = new Customer(i + 1, arrTime, servTime);
			clients.add(c);
		}

	}
	
	public ArrayList<Customer> getClients() {
		return clients;
	}

	public void setClients(ArrayList<Customer> clients) {
		this.clients = clients;
	}

	public Customer getClient(int index) {
		return clients.get(index);
	}

}
