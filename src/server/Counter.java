package server;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;



public class Counter implements Runnable {

	private int ID;
	private BlockingQueue<Customer> clients;

	private boolean running = true;

	public Counter(int ID) {
		this.ID = ID;
		clients = new LinkedBlockingQueue<Customer>();
	}

	@Override
	public void run() {
		while (running) {
			try {
				while (clients.size() > 0) {
					for (Customer client : clients) {
						Thread.sleep(client.getServiceTime() * 500);
						//clients.remove(client);
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void setRunning(boolean b) {
		running = b;
	}

	public void addClient(Customer c) {
		clients.add(c);
	}
	
	public void removeClient(Customer c) {
		clients.remove(c);
	}

	public Customer[] getClientsArray() {
		
		Customer[] clientsArray = new Customer[clients.size()];
		clients.toArray(clientsArray);

		return clientsArray;
	}

	public BlockingQueue<Customer> getClientsList() {
		return clients;
	}

	public int getQueueSize() {
		return clients.size();
	}

	public boolean isEmpty() {
		return clients.isEmpty();
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

}