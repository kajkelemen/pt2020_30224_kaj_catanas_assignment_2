package server;

public class Customer {
	
	 private int noId;
	 private int arrivalTime;
	 private int serviceTime;
	 
	public Customer(int noId, int arrivalTime, int serviceTime) {
		super();
		this.noId = noId;
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
	}
	
	public int getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getServiceTime() {
		return serviceTime;
	}
	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}
	public int getNoId() {
		return noId;
	}
	public void setNoId(int noId) {
		this.noId = noId;
	}

	public String toString() {
		return String.format("ID:%d , aT:%d , sT:%d \n", noId, arrivalTime, serviceTime);
	}

}
