package server;

import java.util.ArrayList;


public class Scheduler {

	private ArrayList<Counter> counters;


	public Scheduler() {
		counters = new ArrayList<Counter>();
	}

	public void sendClientToCounter(Customer c) {

		int minNrOfClients = 1000;
		int index=0;
		for (int i = 0; i < counters.size(); i++) {
			if (counters.get(i).getQueueSize() < minNrOfClients)
			{
				minNrOfClients = counters.get(i).getQueueSize();
				index=i;
			}
		}
		counters.get(index).addClient(c);
		
	}

	public void addCounter(Counter c) {
		counters.add(c);
	}

	public void serveClients() {
		for (Counter c : counters) {
			Thread counterThread = new Thread(c);
			counterThread.start();
		}
	}

	public void stopCounters() {
		for (Counter c : counters) {
			c.setRunning(false);
		}
	}

	public ArrayList<Counter> getCounters() {
		return counters;
	}


}